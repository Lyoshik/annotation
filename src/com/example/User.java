package com.example;

public class User {
    private String login;
    @Encrypted
    private String password;
    User(String login, String password){
        this.login = login;
        this.password = password;
    }
    public String getPassword(){
        return password;
    }
}
