package com.example;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {

        Scanner scanner = new Scanner(System.in);
        CheckUser checkUser = new CheckUser();
        System.out.println("Enter your login:");
        String login =  scanner.next();
        System.out.println("Enter your password:");
        String password = scanner.next();
        User user = new User(login, password);
        checkUser.encrypt(user);

        if (checkUser.checkPassword(user)){
            System.out.println("Login successful!");
        }
        else {
            System.out.println("Login failed!");
        }
    }
}
/*    Пишем мы авторизацию, как будто у нас есть сохраненный, закодированный пароль, мы просим пользователя ввести пароль с клавиатуры, кодируем его и сравниваем с заранее сохраненным, если совпадают - авторизация прошла.

        Нужен класс пользователя, в нем поля логин и пароль.
        Нужна аннотация @Encrypted
Нужен еще один класс со статическим методом который принимает пользователя, ищет поля аннотированные @Encrypted и как то их меняет, неважно как - главное, чтобы всегда одинаково, тоесть из одной строки получалась всегда одинаковая другая строка, но для разных строк эти строки были разные.
        После этого заранее обрабатываем выбранный пароль и пишем его куда то в коде.
        Теперь создаем мейн метод который просит ввести логин пароль, кодируем пользователя и сравниваем закодированный введенный пароль с заранее сохраненным*/
