package com.example;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Base64;

public class CheckUser {
    static final String CODEPASS = "cXdlcnR5";

    public void encrypt(User user) throws IllegalAccessException {
        Field[] fields = user.getClass().getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation: annotations) {
                if (annotation instanceof Encrypted){
                    String password = user.getPassword();
                    field.setAccessible(true);
                    field.set(user, new String(Base64.getEncoder().encode(password.getBytes())));
                }
            }
        }
    }
    public boolean checkPassword(User user){
        return user.getPassword().equals(CODEPASS);
    }



}
